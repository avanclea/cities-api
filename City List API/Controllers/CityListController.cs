﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace City_List_API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CityListController : ControllerBase
    {
        private static readonly string[] CityNames = new[]
        {
            "London", "Toronto", "Austin", "Cape Town", "New Orleans", "Mexico City", "Phnom Penh", "Corvallis", "Memphis", "Lansing"
        };

        private static long RunningID = 0;

        private readonly ILogger<CityListController> _logger;

        public CityListController(ILogger<CityListController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public CityList Get()
        {
            return new CityList
            {
                // ID increments for each CityList
                ID = RunningID++,

                Cities = CityNames
            };
        }
    }
}
