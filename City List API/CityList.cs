using System;

namespace City_List_API
{
    public class CityList
    {
        public long ID { get; set; }

        public string[] Cities { get; set; }
    }
}
